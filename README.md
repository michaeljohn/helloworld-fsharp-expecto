Say hello to Expecto

This is a simple example showing how to use Expecto to write tests in F#.


```sh
$ dotnet run
[21:19:41 INF] EXPECTO? Running tests... <Expecto>
[21:19:42 INF] EXPECTO! 3 tests run in 00:00:00.1733980 for greet – 3 passed, 0 ignored, 0 failed, 0 errored. Success! <Expecto>
```

module SayTests

open Expecto


[<Tests>]
let atest =
  testList "greet" [
    test "greet Bob" {
      let name = "Bob"
      let expected = "Hello Bob"
      let actual = Say.hello name
      Expect.equal expected actual "Failed to say hello to Bob"
    }

    test "greet no one" {
      let name = ""
      let expected = "Hello "
      let actual = Say.hello name
      Expect.equal expected actual "Failed to greet no one"
    }

    test "greet Bob and Alice" {
      let name = "Bob and Alice"
      let expected = "Hello Bob and Alice"
      let actual = Say.hello name
      Expect.equal expected actual "Failed to greet Bob and Alice"
    }
  ]

[<EntryPoint>]
let main argv =
    Tests.runTestsInAssembly defaultConfig argv
